# Política de privacidad y condiciones de servicio
Los responsables de la aplicación `Arribes.net` no se hacen responsables de la exactitud de los datos contenidos en la misma así como de posibles errores de información y los enlaces al contenido de páginas web de terceros. Nos esforzamos en mantener actualizada la información contenida, si bien al disponer de información de establecimientos, estos pueden variar, quedando exentos de cualquier responsabilidad derivada del uso e información de la aplicación `Arribes.net` y desistiendo el usuario la reclamación por daños y perjuicios.

## Cookies y seguimiento
La aplicación no utiliza cookies de ningún tipo ni hace seguimiento de tus acciones fuera de la aplicación.
Dentro de la aplicación se realiza únicamente con fines de analítica un seguimiento de los puntos de interés, localidades y negocios más visitados. Estos datos se envían a nuestro servidor y este a su vez envía estos datos a `Google Analytics`. Estos datos siempre son recogidos de forma anónima.

## Permisos
La aplicación solicitará el permiso de ubicación para poder mostrar lugares cercanos en la función 'Cerca de mí'. Además se podrán activar opcionalmente las notificaciones que serán utilizadas únicamente por la aplicación para mostrar lugares y establecimientos cercanos. Todos los datos solicitados al navegador se utilizan únicamente en tu dispositivo y no se envían de forma externa.
### Ubicación
Para determinar tu ubicación y sugerirte lugares cercanos usamos una API de tu navegador, lo que quiere decir que usaremos únicamente los servicios de ubicación de tu dispositivo (siempre y cuando nos hayas dado permiso).
### Notificaciones
Opcionalmente podemos notificarte de los lugares cercanos a tí si activas el permiso de notificaciones. Estas notificaciones son locales, lo que quiere decir que es la aplicación misma la que las envía, nunca usaremos un servidor externo para este servicio, ya que requiere de tu ubicación.

### API de conexión
Usamos la API de conexión del navegador para identificar la velocidad de tu red sin hacer ninguna prueba y así poder decidir el contenido que se puede cargar sin lastrar el rendimiento de la aplicación.

## Servicios externos
Usamos la `Google Maps API` para proporcionarte los mapas de la zona cercana a tí y marcar los puntos de interés en el mapa.
Además tenemos enlaces que te llevan a páginas externas a la aplicación. Estas páginas tienen sus propias políticas de privacidad y condiciones de servicio, por lo que no nos hacemos responsables del tratamiento de información que lleven a cabo las páginas de terceros.
También usamos `YouTube` para mostrar videoguías en su sección correspondiente. La política de nuestra página no abarca YouTube, de modo que las interacciones con YouTube incluida la reproducción de vídeos, están regidas por la Política de privacidad y las condiciones de uso de YouTube.

## Seguridad y auditoría
El código de la aplicación está liberado bajo licencia GPL-v2 y disponible en [GitLab](https://gitlab.com/mrvikclase/arribes.net).
Las libertades están recogidas en la [licencia](https://gitlab.com/mrvikclase/arribes.net/blob/master/LICENSE).

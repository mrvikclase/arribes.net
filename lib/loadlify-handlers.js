let handlers=loadlify.handlers;
handlers["application/json"]=function(a, b){
	let salida={};
	try{
		salida.data=JSON.parse(a.data);
		salida.ok=true;
	}catch(e){
		salida.text=a.data;
		salida.ok=false;
		salida.data=undefined;
	}
	return [salida, {flags:b,url:a.url}];
}
handlers["text/markdown"]=async function(a,b){
	let converter=await loadlify.load("showdown",["nodemodule"]).then(module=>{
		return new module.exports.showdown.Converter();
	});
	converter.setFlavor("github");
	salida={
		md: a.data,
		html: converter.makeHtml(a.data)
	};
	return [salida, {flags:b,url:a.url}]
}

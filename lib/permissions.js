class permissions{
	constructor(){
		throw new Error("Esta es una clase estática");
	}
	static async location(stop){
		let ok;
		if(typeof cordova!="undefined"){
			let pr=await new Promise((resolver, rechazar)=>{
				let perm=cordova.plugins.permissions;
				perm.checkPermission([perm.ACCESS_COARSE_LOCATION, perm.ACCESS_FINE_LOCATION], resolver, rechazar);
			});
			ok=pr.hasPermission;
		}else{
			let {state}=await navigator.permissions.query({name: 'geolocation'});
			ok=state;
		}
		if(ok)return ok;
		if(typeof cordova == "undefined"){ //BUG Cordova test results are not accurate, need to require permissions
			if(stop)return ok;
		}
		if(typeof cordova!="undefined"){
			return await permissions.getCordova("location");
		}else{
			return exports.events.followme();
		}
	}
	static async notification(stop){
		if(typeof Notification=="undefined")return false;
		let ok=((Notification.permission=="granted") ? true : false);
		if(ok)return ok;
		if(stop)return ok;
		return await permissions.getWorker("notification");
	}
	static async getWorker(a){
		switch(a){
			case "notification":
				return await Notification.requestPermission();
			default:
				return undefined;
		}
	}
	static async getCordova(a){
		switch(cordova.platformId){
			case 'android':
				let permisos={
					location: ["ACCESS_COARSE_LOCATION", "ACCESS_FINE_LOCATION"]
				};
				let perm=cordova.plugins.permissions;
				let list=[];
				permisos[a].forEach(a=>{
					list.push(perm[a]);
				});
				let salida=await new Promise((resolver, rechazar)=>{
					perm.requestPermissions(list,resolver,rechazar);
				});
				return salida.hasPermission;
				break;
			default:
				throw new Error("Unsupported platform: ",cordova.platformId);
		}
		
	}
	static async updateIndex(){
		let ok=["granted",true];
		self.exports.data.permisos={
			geolocation: ok.includes(await exports.permissions.location()),
			notification: await exports.permissions.notification(true)=="granted"
		};
		self.mdcontrol.topAppBar.aditional.opciones.forEach(el=>{
			if(!el["permission-request"])return;
			if(self.exports.data.permisos.hasOwnProperty(el["permission-request"])){
				let sitios=exports.data.sitios.filter(a=>a.type=="view");
				let rt=exports.util.ArrayUtil.indexOfProp({prop: "name",array: sitios,val:app.$route.name});
				if(rt==undefined)return;
				let route=sitios[rt];
				if(route.permissions&&route.permissions.includes(el["permission-request"])){
					el.hidden=self.exports.data.permisos[el["permission-request"]];
				}else{
					el.hidden=true;
				}
			}
		});
	}
}

if(typeof exports!="undefined"){
	exports.permissions=permissions;
}

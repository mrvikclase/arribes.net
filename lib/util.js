class LocationUtil{
	constructor(){
		throw new Error("La clase es estática");
	}
	
	static async getImgFromLocation(a){
		let src=`https://maps.googleapis.com/maps/api/staticmap?center=${a.latitude},${a.longitude}&zoom=20&size=${[innerWidth,Math.floor(innerHeight/2)].join("x")}&sensor=false`;
		return await load(src, ["type: text/plain", "notext"]);
	}
}
class AlertUtil{
	constructor(){
		throw new Error("Esta es una clase estática");
	}
	static async alert(a){
		return swal(a);
	}
	static async confirm(a){
		let {value}=await swal({
			title: a.title,
			text: a.text,
			showCancelButton: true,
			confirmButtonText: a.confirm||"Aceptar",
			cancelButtonText: a.cancel||"Cancelar"
		});
	}
	static error(a){
		if(typeof a=="object"){
			a.type="error";
			return AlertUtil.alert(a);
		}
		return AlertUtil.alert({
			type: "error",
			title: "Error",
			text: a
		});
	}
	static warn(a){
		if(typeof(a)=="object"){
			a.type="warning";
			return AlertUtil.alert(a);
		}
		return AlertUtil.alert({
			type: "warning",
			title: "Advertencia",
			text: a
		});
	}
}
class TextUtil{
	constructor(){
		throw new Error("Clase estática");
	}
	static async parseHTML(a){
		var element = document.createElement('div');
		function decodeHTMLEntities (str) {
			if(str && typeof str === 'string') {
			// strip script/html tags
			str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
			str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
			element.innerHTML = str;
			str = element.textContent;
			element.textContent = '';
			}
			return str;
		}
		return decodeHTMLEntities(a);
	}
	static async purgueHTML(a){
		let str=a;
		let el=document.createElement("div");
		
		str=str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, ''); //Remove Script tags before start...
		str=str.replace(/<img.*>/g, ''); //Remove images before start...
		
		el.innerHTML=str;
		let selector=el.querySelectorAll("object,script,img,[src]");
		selector.forEach(node=>node.remove());
		if(el.innerHTML.match("roja"))debugger;
		let rt=el.innerHTML;
		el.remove();
		return rt;
	}
}
class Connection{
	constructor(){
		//NOOP
	}
	get online(){
		let oktypes=["3g", "4g"];
		if(navigator.onLine&&oktypes.includes(navigator.connection.effectiveType)){
			return true;
		}else{
			return false;
		}
	}
}
class ArrayUtil{
	constructor(){
		throw new Error("Clase estática");
	}
	static indexOfProp(arg){
		let {array,prop,val}=arg;
		for(let i in array){
			if(array[i][prop]==val)return i;
		}
		return undefined;
	}
}
class GoogleMapsUtil{
	constructor(){
		throw new Error("La clase es estática");
	}
	static async remark(map){
		if(!self.google||!google.maps)throw new Error("Google maps is not present");
		if(!map)throw new Error("The map argument is mandatory and wasn't provided");
		if(map.app&&map.app.markers&&map.app.markers.length>0){
			map.app.markers.forEach((marker,index)=>{
				marker.setMap(null);
				delete map.app.markers[index];
			});
		}
		if(!map.app)map.app={markers:[]};
		if(!map.app.markers)map.app.markers=[];
		await self.exports.api.dbQuery.lock;
		let data=self.exports.data.nearby;
		if(!data&&app.$route.query.loc)data=await self.exports.api.dbQuery.nearby(app.$route.query.loc);
		
		if(!data&&app.$route.path=="/nearby")return;
		if(data.forEach)self.exports.data.nearby.forEach(nearbyIterator);
		Object.keys(data).forEach(i=>{
			nearbyIterator(data[i]);
		});
		
		function nearbyIterator(nearbyItem){
			let {coordenadas}=nearbyItem;
			coordenadas=self.exports.api.distance.parseCoordinates(coordenadas);
			if(!coordenadas)return console.warn("Received an item without coordinates: ",nearbyItem);
			let position=new google.maps.LatLng(coordenadas.latitude, coordenadas.longitude);
			let icon;
			if(nearbyItem.iconPath&&nearbyItem.icon){
				let size="svg/production";
				icon=`node_modules/material-design-icons/${nearbyItem.iconPath}/${size}/ic_${nearbyItem.icon}_24px.svg`;
			}
			let marker=new google.maps.Marker({
				position,
				map,
				icon,
				title: nearbyItem.nombre,
				animation: google.maps.Animation.DROP //Let's give some style
			});
			marker.addListener("click", originalEvent=>{
				let event={
					marker,
					originalEvent,
					item: nearbyItem
				};
				return self.exports.events.googleMapsClick(event);
			});
			map.app.markers.push(marker);
		}
		if(app.$route.query.loc){
			var {nombre,coordenadas}=await self.exports.api.dbQuery.localidades(app.$route.query.loc);
			coordenadas=self.exports.api.distance.parseCoordinates(coordenadas);
			var position=new google.maps.LatLng(coordenadas.latitude, coordenadas.longitude);
			map.setCenter(position);
			if(map.app.localidad){
				map.app.localidad.setPosition(position);
			}else{
				map.app.localidad=new google.maps.Marker({
					position,
					map,
					title: nombre,
					animation: google.maps.Animation.DROP
				});
			}
		}
	}
}

class VoiceUtil{
	constructor(){
		throw new Error("La clase es estática");
	}
	static speech(text){
		let rt;
		if(typeof TTS!="undefined"){
			rt=new Promise((resolve,reject)=>{
				self.TTSSpeaking=true;
				TTS.speak({
					text,
					locale: "es-ES" //navigator.language //No hay traducción
				},a=>{
					self.TTSSpeaking=false;
					resolve(a);
				},a=>{
					self.TTSSpeaking=false;
					reject(a);
				});
			});
		}else{
			let msg=new SpeechSynthesisUtterance(text);
			rt=new Promise(resolver=>{
				msg.onend=resolver;
			});
			speechSynthesis.speak(msg);
		}
		return rt;
	}
}
class Random{
	static gen(min,max){
        return Math.floor(Math.random() * (max - min + 1)) + min;
	}
}

if(exports){
	exports.util={
		LocationUtil,
		TextUtil,
		AlertUtil,
		ArrayUtil,
		GoogleMapsUtil,
		VoiceUtil,
        Random,
		connection: new Connection()
	};
}

let mdc=exports.mdc;
self.mdcontrol={};
function select(a){
	return document.querySelector(a);
}
class mdcontrolClass{
	constructor(){
		throw new Error("La clase es estática");
	}
	static async initTopAppBar(){
		let barEl=select("#top-appBar");
		let drawerEl=select("#app-drawer");
		if(!barEl||!drawerEl)throw new Error(`Cannot init the application shell. The app bar is ${typeof barEl} and the drawer is ${typeof drawerEl}`);
		
		mdcontrol.drawer=new mdc.drawer.MDCTemporaryDrawer(drawerEl);
		mdcontrol.topAppBar=mdc.topAppBar.MDCTopAppBar.attachTo(barEl);
		select("[data-action='open-drawer']").addEventListener("click", event=>{
			event.preventDefault();
			mdcontrol.drawer.open=true;
		});
		let backEl=select("[data-action='go-back']");
		if(backEl)backEl.addEventListener("click", event=>{
			event.preventDefault();
			self.exports.api.history.back();
		});
		mdcontrol.topAppBar.vistas=new Vue({
			el: "#controles-vistas",
			methods:{
				go: exports.events.botonesInicio
			},
			data:{sitios: exports.data.sitios}
		});
		mdcontrol.topAppBar.aditional=new Vue({
			el: "#controles-adicionales",
			methods:{
				controlesAdicionales: self.exports.events.controlesAdicionales
			},
			data:{
				opciones:[
					{
						action: "followme",
						icon: "location_searching",
						hidden: false,
						["permission-request"]: "geolocation"
					},{
						action: "goBack",
						icon: "arrow_back",
						hidden:false
					}
				]
			}
		});
		
		await mdcontrolClass.autoInit(barEl, ()=>{});
		return {drawer: mdcontrol.drawer, topAppBar: mdcontrol.topAppBar};
	}
	static autoInit(a, b){
		return new Promise(resolver=>{
			document.addEventListener("MDCAutoInit:End", resolver);
			mdc.autoInit(a,b);
		});
	}
	static async autoInitEvents(){
		let sel=document.querySelectorAll("[data-mdc-auto-init][data-events]");
		sel.forEach(el=>{
			let evts=el.getAttribute("data-events").split(",");
			evts.forEach(evt=>{
				let fn=new Function(["target","event"],el.getAttribute("data-events-"+evt));
				let toListen=el[el.getAttribute("data-mdc-auto-init")];
				if(!toListen){
					console.error("Events auto init caught an intruder ", toListen);
					return;
				} //[data-events="change"] [data-events-change="console.log(target)"]
				toListen.listen(evt, event=>{
					return fn(el,event);
				});
			});
		});
	}
	static async todoFloatLabel(){
		let elArray=document.querySelectorAll(".mdc-select[data-mdc-auto-init='MDCSelect']");
		if(!elArray.length){
			return;
		}
		elArray.forEach(el=>{
			if(el&&el.MDCSelect){
				el.MDCSelect.label_.float(true);
			}else{
				console.warn("Non autoinit item",el);
			}
		});
	}
	static async initDialogs(){
		let template=await load("components/promo.vue","noprefix").then(r=>r.text);
		let data={
			establecimiento: self.exports.data.todo[0]||{}
		};
		let el="#mdcontrol-dialog";
		self.mdcontrol.dialogs=new Vue({
			el,
			template,
			data,
			components: exports.data.components
		});
		await Vue.nextTick();
		
		el=select("[data-arribes='promo-dialog']");
		
		await mdcontrolClass.autoInit();
		
		let {MDCDialog}=el;
		self.mdcontrol.dialogs.$options.MDCDialog=MDCDialog;
		
		return self.mdcontrol.dialogs;
	}
	static async showDialog(el){
		var dialog=self.mdcontrol.dialogs.$options.MDCDialog;
		if(!dialog)throw new Error("Dialog is not defined");
		let p=new Promise((resolve,reject)=>{
			dialog.listen("MDCDialog:accept",()=>{
				return resolve(true);
			});
			dialog.listen("MDCDialog:cancel",()=>{
				return resolve(false);
			});
		});
        self.mdcontrol.dialogs.establecimiento=el;
		dialog.show();
		return await p;
	}
}
exports.mdcontrol=mdcontrolClass;

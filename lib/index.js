if(typeof exports!="undefined"){
	exports.index={};
	exports.data={nearby:[], todo:[], localidades:[], online: (navigator.onLine)?true:false,
		permisos:{}, nocoords:[],cron:{},failed:[], videoguías:[],
		responsabilidad: "",legal:"",policyOk: localStorage.policyOk,
		components:{},
		nearbyDist: 25*1000 //KMs
	};
}

async function createApp(){
	let sitios=[
		{
			title: "Inicio",
			type:"view",
			name: "inicio",
			icon: "home",
			topAppBar: true,
			drawer: true
		},{
			order_link:1,
			title: "Cerca de mí",
			type:"view",
			name: "nearby",
			icon: "my_location",
			icons: ["followme"],
			permissions: ["geolocation"],
			drawer: true,
			inicio: true,
			topAppBar: true,
			inicio_head:true
		},{
			order_link:2,
			title: "Videoguías",
			type: "view",
			name: "videoguias",
			icon: "ondemand_video",
			drawer: true,
			inicio: true,
			inicio_head:true
		},{
			order_link:3,
			title: "Poblaciones",
			type: "view",
			name: "todo",
			icon: "place",
			drawer: true,
			inicio: true
		},{
			order_link:4,
			order_nearby: 1,
			title: "Qué ver",
			type: "link",
			route: {
				path: "todo",
				query:{cat: "Qué ver"}
			},
			icon: "photo_camera",
			inicio: true
		},{
			order_link:5,
			order_nearby:2,
			title: "Actividades",
			type:"link",
			route:{
				path: "todo",
				query:{cat: "Actividades"}
			},
			icon: "directions_boat",
			inicio: true
		},{
			order_link:6,
			order_nearby:4,
			title: "Restaurantes",
			type: "link",
			route:{
				path: "todo",
				query:{cat: "Restaurantes"}
			},
			icon: "restaurant",
			inicio: true
		},{
			order_link:7,
			order_nearby:5,
			title: "Alojamientos",
			type: "link",
			route:{
				path: "todo",
				query:{cat: "Alojamiento"}
			},
			icon: "hotel",
			inicio: true
		},{
			order_link:8,
			order_nearby:3,
			title: "Productos",
			type: "link",
			route:{
				path: "todo",
				query:{cat:"Productos"}
			},
			icon: "shopping_basket",
			inicio: true
		},{
			name: "vista",
			type: "view"
		},{
			name:"responsabilidad",
			type: "view"
		},{
			name: "legal",
			type: "view"
		}
	];
	sitios.forEach(sitio=>{
		if(sitio.topAppBar)sitio.here=false;
	});
	exports.data.sitios=sitios;
	
	//Load components
	let componentsArray=[
		{
			url: "lista.vue",
			name: "list-item"
		},{
			url: "mapa.vue",
			name: "google-map"
		},{
			url: "detalles.vue",
			name: "detalles"
		},{
			url: "icon-button.vue",
			name: "icon-button"
		},{
			url: "image-gallery.vue",
			name: "image-gallery"
		},{
			url: "video-element.vue",
			name: "video-element"
		},{
			url: "valoracion.vue",
			name: "valoracion"
		}
	];
	let components={};
	for(let i in componentsArray){
		if(!componentsArray.hasOwnProperty(i))continue;
		let component=componentsArray[i];
		let {text,link}=await load(`components/${component.url}`, ["noprefix", "type: text/plain"]);
		if(!text){
			let err=new Error(`Component has no data: ${component.name}. Tried to load from ${link.href}`);
			console.error(err);
			continue;
		}
		let comp=[component.name,{
			template: text,
			props: ["item", "options"],
			components
		}];
		components[comp[0]]=comp[1];
	}
	exports.data.components=components;
	
	let routes=[];
	for(let i in sitios){
		if(!sitios.hasOwnProperty(i))continue;
		let sitio=sitios[i];
		if(sitio.type!="view")continue;
		
		if(sitio.icons)sitio.icons.push["goBack"];
		if(!sitio.icons)sitio.icons=["goBack"];
		let loading=await load(`templates/${sitio.name}.vue`, ["noprefix", "type: text/plain"]);
		let template=loading.text;
		let props=["data"];
		let ruta={
			name: sitio.name,
			path: sitio.path||`/${sitio.name}`,
			component: {template, icons: sitio.icons, components,props}
		};
		routes.push(ruta);
	}
	//RUTAS CREADAS
	Vue.use(VueRouter);
	let router=new VueRouter({
		methods: self.exports,
		routes,
		data: exports.data
	});
	let app=new Vue({
		router,
		data: exports.data
	}).$mount("#app");
	
	return app;
}
//Parte de orquestación
(async function(){
	//Early events
	window.onerror=exports.events.globalError;
	window.onunhandledrejection=exports.events.globalError;
	
	let inicio,start,last;
	console.log("Starting load of application...");
	inicio=performance.now();
    
    //Cordova plugin config
    if(typeof cordova!="undefined"&&typeof plugins!="undefined"){
        if(plugins.headerColor)plugins.headerColor.tint("#387002");
    }
	
	if(localStorage.policyOk){
		start=performance.now();
		await exports.api.dbQuery.lock; //Await for the db to be ready
		console.log(`DB initialized in ${performance.now()-start}ms`);
	
		//Populate data
		start=performance.now();
		if(localStorage.coords){
			if(typeof localStorage.coords=="object"){
				exports.data.coords=localStorage.coords;
			}else{
				try{
					exports.data.coords=JSON.parse(localStorage.coords);
				}catch(e){
					console.warn(e);
				}
			}
		}
		exports.data.todo=await exports.api.dbQuery.all();
		exports.data.localidades=await exports.api.dbQuery.localidades();
		exports.data.videoguías=await exports.api.dbQuery.videoguías();
		console.log(`Data populated in ${performance.now()-start}ms`);
	}else{
		console.log("Agreement not accepted");
	}
	
	//Crear la app
	start=performance.now();
	self.app=await createApp();
	console.log(`App created in ${performance.now()-start}ms`);
	
	//APP shell style init
	start=performance.now();
	let {topAppBar}=await exports.mdcontrol.initTopAppBar();
	exports.index.topOptions=topAppBar.aditional.opciones;
	let dialogs=await exports.mdcontrol.initDialogs();
	console.log(`Shell style initialized in ${performance.now()-start}ms`);
	
	if(localStorage.policyOk){
		start=performance.now();
		exports.permissions.updateIndex(); //Update app permissions
		console.log(`Permissions updated in ${performance.now()-start}ms`);
	}
	
	//Eventos de la APP
	start=performance.now();
	self.app.$router.afterEach(exports.events.afterRouteChange);
	exports.events.afterRouteChange();
	navigator.connection.onchange=exports.events.connectionchange;
	console.log(`Created events in ${performance.now()-start}ms`);
	
	window.dispatchEvent(window.appReadyEvt); //App initialization done
	console.log(`Init function took ${performance.now()-inicio}ms`);
	exports.data.responsabilidad=await load("README.md", ["type: text/markdown","noprefix","noflagsindeps"]).then(m=>{
		return m.apply[0].html;
	});
	exports.data.legal=await load("LEGAL.md", ["type: text/markdown","noprefix","noflagsindeps"]).then(a=>a.apply[0].html);
	return;
})()
.catch(e=>{
	if(exports.events){
		throw e;
	}else{
		if(exports.util)exports.util.AlertUtil.error({
			title: "Error de la APP",
			text: "Ha ocurrido un error al iniciar la APP",
			footer: e.message
		});
		if(!exports.util)alert("Fallo en la aplicación: "+e.message)
		throw e; //Rethrow errors
	}
})

if(exports){
	exports.index={};
}

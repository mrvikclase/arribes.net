class events{
	constructor(){
		throw new Error("This class is static");
	}
	static appReady(){
		let el=document.querySelector("#prestyle");
		let el2=document.querySelector("#loading-splash");
		if(el!=null)el.remove();
		if(el2!=null)el2.remove();
	}
	
	static async locationchange(...args){
		let coords=args[0].coords;
		if(self.exports.data)self.exports.data.coords={latitude: Number(coords.latitude.toString()), longitude: Number(coords.longitude.toString())};
		if(typeof google!="undefined"&&self.map){
			let gloc=new google.maps.LatLng(coords.latitude, coords.longitude);
			if(app.$route.path=="/nearby")map.setCenter(gloc);
			if(self.map.app.marker){
				self.map.app.marker.setPosition(gloc);
			}else{
				self.map.app.marker=new google.maps.Marker({
					map: self.map,
					position: map.center,
					title: "Estás aquí"
				});
			}
		}
		self.exports.api.dbQuery.lock.then(async X=>{
			if(self.exports.data&&self.app.$route.path=="/nearby"){
				let c={latitude: Number(coords.latitude.toString()), longitude: Number(coords.longitude.toString())};
				self.exports.data.coords=c;
				self.exports.data.nearby=await self.exports.api.dbQuery.nearby(c);
				if(self.map)self.exports.util.GoogleMapsUtil.remark(self.map);
			}
		});
		if(exports.data&&exports.data.coords)localStorage.setItem("coords",JSON.stringify(exports.data.coords));
	}
	
	static async afterRouteChange(){
		if(!localStorage.policyOk&&app.$route.name!="responsabilidad")app.$router.push("/responsabilidad");
		if(app.$route.path=="/")return app.$router.push("/inicio");
		let noscroll=[];
		if(!noscroll.includes(app.$route.path))scroll(0,0);
		let changeIcon=new Promise(resolver=>{ //Top app bar icon updater
			let el=app.$route.path;
			for(let i in app.$router.options.routes){
				let route=app.$router.options.routes[i];
				if(route.path==el){
					if(!route.component.icons)return self.exports.index.topOptions.forEach(a=>a.hidden=true);
					route.component.icons.forEach(icon=>{
						self.exports.index.topOptions.forEach(icon2=>{
							if(icon2.action==icon){
								icon2.hidden=false;
							}else{
								icon2.hidden=true;
							}
						});
					});
				}
			}
		});
		(async ()=>{
			let routes=exports.data.sitios.filter(a=>a.type=="view");
			let rt=exports.util.ArrayUtil.indexOfProp({prop: 'name',val:app.$route.name,array:routes});
			if(!rt)return;
			let route=routes[rt];
			if(!route.permissions)return;
			let a=[];
			route.permissions.forEach(p=>{
				if(p=="geolocation"){
					events.followme();
				}
				let fn=exports.permissions[p];
				if(!fn)return;
				a.push(fn());
			});
			return await Promise.all(a);
		})();
		(async ()=>{
			if(!app.$route.name)return;
			exports.data.sitios.forEach(sitio=>{
				switch(sitio.name){
					case app.$route.name:
						return sitio.here=true;
					default:
						return sitio.here=false;
				}
			});
		})();
// 		console.log("Ha cambiado la ruta a: ", app.$route.path);
		await Vue.nextTick();
		if(self.exports.data&&app.$route.path=="/todo"){
			if(app.$route.query.loc){
				var nearbyData=self.exports.api.dbQuery.nearbyFrom(app.$route.query.loc).then(data=>{
					if(!data)return self.exports.data.nearby=[] //Refuse to write undefined (brokes Vue)
					self.exports.data.nearby=data;
				});
			}else{
				self.exports.data.nearby=await exports.api.dbQuery.all();
			}
		}
		if(document.querySelector("[data-map='container']")&&self.exports.util.connection.online){
			let el=document.querySelector("[data-map='container']");
			if(el){
				el.setAttribute("data-map-visible", "waiting");
				load("google-maps-js").then(async loaded=>{ //INIT GOOGLE MAPS MAP
					if(el.getAttribute("data-map-visible")=="true"&&self.map){
						return await exports.util.GoogleMapsUtil.remark(map); //If the map is initialized, re-create the markers
					}
					if(el.hasAttribute("data-map-center")&&el.getAttribute("data-map-center")){
						var coords1={coords: exports.api.distance.parseCoordinates(el.getAttribute("data-map-center"))};
						var coords=new google.maps.LatLng(coords1.coords.latitude, coords1.coords.longitude);
					}else if(app.$route.path=="/nearby"){ //Ok, map will be centered to current location
						if(typeof cordova == "undefined"){
							if((await self.exports.permissions.location(true))!="granted")return;
						}
						if(!self.exports.data.coords){
							var coords1=await self.exports.api.LocationAPI.location();
							var coords=new google.maps.LatLng(coords1.latitude, coords1.longitude);
						}else{
							var coords=(self.exports.data.coords instanceof google.maps.LatLng)?self.exports.data.coords:undefined;
							if(!coords){
								let parsed=self.exports.api.distance.parseCoordinates(self.exports.data.coords);
								coords=new google.maps.LatLng(parsed.latitude,parsed.longitude);
							}
							var coords1={coords:self.exports.api.distance.parseCoordinates(coords)};
						}
					}else if(app.$route.query.loc){ //Map will be centered to the current selected option
						let {coordenadas}=await self.exports.api.dbQuery.localidades(app.$route.query.loc);
						var coords1={coords:self.exports.api.distance.parseCoordinates(coordenadas)};
						var coords=new google.maps.LatLng(coords1.coords.latitude,coords1.coords.longitude);
					}
					let center=coords;
					self.map=new google.maps.Map(document.querySelector("[data-map-inner='map']"), {
						center,
						zoom: 12,
						mapTypeId: "terrain",
						styles:[
							{
								featureType: "poi",
								elementType: "labels",
								stylers:[{
									visibility: "off" //Eliminar la visibilidad de los puntos de interes.
								}]
							}
						]
					});
					self.map.app={};
					el.setAttribute("data-map-visible", "true");//let the navigator start rendering the mapº
					
					if(app.$route.path=="/nearby"){
						if(!self.exports.api.locationAPI)self.exports.api.locationAPI=new self.exports.api.LocationAPI({onchange: exports.events.locationchange, onerror: console.error});
						await self.exports.events.locationchange(coords1);
						self.exports.util.GoogleMapsUtil.remark(map);
					}else if(app.$route.path=="/todo"){
						await nearbyData;
						self.exports.util.GoogleMapsUtil.remark(self.map);
					}else if(app.$route.path=="/vista"){
						if(map.app.marker)map.app.marker.setPosition(map.center);
						if(!map.app.marker)map.app.marker=new google.maps.Marker({
							position: map.center,
							map,
							title: "Establecimiento seleccionado",
						});
					}
				});
			}else{
				if(app.$route.path=="/nearby"&&(await self.exports.permissions.location(true))=="granted"){
					if(!self.exports.api.locationAPI)self.exports.api.locationAPI=new self.exports.api
						.LocationAPI({onchange: exports.events.locationchange, onerror: console.error});
				}
			}
		}
		{
			let el=document.querySelector("[data-gallery='overall-container']");
			if(el)events.autoMoveGallery();
		}
		load("api.js").then(a=>{
			a.exports.api.history.push(location.hash);
		});
		if(!navigator.doNotTrack&&exports.data.online){
			if(exports.api)exports.api.dbQuery.trackMove(location.hash.substr(1)).catch(e=>{
				//Who cares!
			})
		}
		mdcontrol.drawer.open=false;
		let selector=document.querySelector("#app-view");
		if(!selector)return true;
		await Vue.nextTick();
		let rt=await self.exports.mdcontrol.autoInit(selector, ()=>{});
		await events.updateGalleryImage();
		if(exports.permissions)await self.exports.permissions.updateIndex();
		await self.exports.mdcontrol.autoInitEvents();
		await Vue.nextTick();
		if(app.$route.path=="/todo")self.exports.mdcontrol.todoFloatLabel();
        
        //Publicidad de establecimientos destacados
        if(app.$route.query.loc){
            if(nearbyData)await nearbyData;
            let el=await exports.api.dbQuery.destacado(app.$route.query.loc);
            exports.mdcontrol.showDialog(el).then(b=>{
                if(b)self.app.$router.push(`/vista?id=${el.id}`);
            });
        }
		return rt;
	}
	static async controlesAdicionales(event){
		let action=event.target.getAttribute("data-action");
		if(exports.events[action])return exports.events[action](event);
		console.warn("Uncaught action "+action);
	}
	static async connectionchange(event){
		if(!event||!event.target)var event={target: navigator.connection};
		if(exports.data)exports.data.online=self.exports.util.connection.online;
	}
	static async followme(){
		try{
			if(!self.exports.api.LocationAPI.checkLocation) throw new Error("Tu navegador no soporta la localización");
		}catch(e){
			self.exports.util.AlertUtil.error(e);
		}
		let location=await self.exports.permissions.location(true);
		if(location=="prompt"){
			if(typeof cordova!="undefined"){ //Get permission from cordova plugin
				self.exports.permissions.location();
			}else{
				let loc=await self.exports.api.LocationAPI.location();
				events.locationchange(loc);//Don't shred the location object
			}
			events.afterRouteChange(); //Should restart map after permission grant
		}else if(location=="denied"){
			self.exports.util.AlertUtil.error("El permiso de localización está denegado. Deberás activarlo para poder ver lo que hay cerca de tí");
		}
		let notify=await self.exports.permissions.notification(true);
		if(notify=="prompt"||notify=="default"){
			let setNotifications=await self.exports.util.AlertUtil.confirm({
				type: "question",
				title: "Permiso de notificación",
				text: "Puedo mostrarte lugares interesantes según te desplazas. ¿Quieres que te envíe este tipo de notificaciones?"
			});
			if(setNotifications)self.exports.permissions.notification();
		}
		if(notify=="denied")self.exports.util.AlertUtil.warn({
			title: "Permiso de notificación",
			text: "El permiso de notificación ha sido denegado. No podré notificarte de lo que hay cerca de tí."
		});
		await self.exports.permissions.updateIndex();
	}
	static async goBack(event){
		return app.$router.back();
	}
	static async botonesInicio(option){
		if(option.route)return app.$router.push(option.route);
		return app.$router.push(option.name);
	}
	static async filtroLocalidad(localidad){
		return app.$router.push({query: {loc: localidad}});
	}
	static async listaTodoClick(item){
		return app.$router.push({
			path: "/vista",
			query: {id: item.id}
		});
	}
	static async filterEvent(a,b){
		let navigation={path: "/todo", query: {}};
		for(let i in app.$route.query){
			navigation.query[i]=app.$route.query[i];
		}
		navigation.query[a]=b;
		return app.$router.push(navigation);
	}
	static async googleMapsClick(event){
		events.listaTodoClick(event.item);
	}
	static async mostrarCategoria(sitio){
		let el=document.querySelector(`[data-todo-cat-name='${sitio}']`);
		if(!el)return;
		let el2=el.parentElement.firstElementChild;
		
		let clasesOpen=["animated", "bounceInLeft", "open"];
		let clasesClose=["animated", "bounceOutRight"];
		let clases=clasesOpen.concat(clasesClose);
		
		let action=clasesOpen;
		if(el.classList.contains(clasesOpen[1]))action=clasesClose;
		
		clases.forEach(a=>(a!=clasesOpen[0])?el.classList.remove(a):false);
		action.forEach(clase=>el.classList.add(clase));
		
		(el2.classList.contains("open"))?el2.classList.remove("open"):el2.classList.add("open");
	}
	static async reservar(a){
		return location.href=a.reserva;
	}
	static async globalError(e){
		console.warn("Unhandled error or rejection");
        if(e.stack)console.error("Error stack: ",e.stack);
		let error=(e.message)?e:e.reason;
		console.error(error);
		if(exports.util&&exports.util.AlertUtil){
			exports.util.AlertUtil.error({
				title: "Error de la APP",
				text: "Ha ocurrido un error al iniciar la APP",
				footer: error.message
			});
		}else{
			alert("Error en la aplicación: "+error.message);
		}
	}
	static async ttsEvent(text){
		if(!text)return;
		if(typeof TTS=="undefined"){
			if(self.speechSynthesis.speaking)return self.speechSynthesis.cancel();
		}else if(self.TTSSpeaking){
			return exports.util.VoiceUtil.speech("");
		}
		self.exports.util.VoiceUtil.speech(text);
	}
	static async updateGalleryImage(a){
		let el=document.querySelector("[data-gallery='overall-container']");
		if(!el)return;
		let selectors={
			image: "data-gallery-image",
			dot: "data-gallery-dot"
		};
		let tiempo=2000;//ms
		if(a&&el.querySelectorAll(`[${selectors.image}]`).length<a)return;
		el.querySelectorAll(`[${selectors.image}],[${selectors.dot}]`).forEach(it=>it.classList.remove("active"));
		if(!a){
			el.querySelector(`[${selectors.image}='0']`).classList.add("active");
			el.querySelector(`[${selectors.dot}='0']`).classList.add("active");
			return el;
		}else{
			let im=el.querySelector(`[${selectors.image}='${a}']`);
			let dot=el.querySelector(`[${selectors.dot}='${a}']`);
			if(im)im.classList.add("active");
			if(dot)dot.classList.add("active");
		}
	}
	static async moveGallery(amount=1){
		let el=document.querySelector("[data-gallery='overall-container']");
		if(!el)throw new Error("Where is the slide?");
		let q=el.querySelectorAll("[data-gallery-dot]");
		let act=0;
		let actDot=el.querySelector("[data-gallery-dot].active");
		if(actDot)act=actDot.getAttribute("data-gallery-dot")||0;
		
		let salida=Number(act)+amount;
		if(salida>=q.length)salida=0;
		if(salida<=0)salida=0;
		return events.updateGalleryImage(salida);
	}
	static autoMoveGallery(){
		let time=4000;
		if(exports.data.cron.autoMoveGallery)clearInterval(exports.data.cron.autoMoveGallery);
		exports.data.cron.autoMoveGallery=self.setInterval(()=>{
			events.moveGallery().catch(e=>{
				clearInterval(exports.data.cron.autoMoveGallery);
			});
		},time);
	}
	static async imgFailed(event){
		let el=event.srcElement;
		if(!el)return;
		el.classList.add("failed"); //Hide the image as soon as possible
		let url=event.srcElement.getAttribute("src");
		exports.data.failed.push(url);
		console.error(url+" load failed :(");
	}
	static async goMaps(url){
		location.href=url;
	}
	static async responsabilidad(bool){
		if(bool){
			localStorage.setItem("policyOk",true);
			app.$router.push("/inicio");
			location.reload();
		}else{
			localStorage.removeItem("policyOk");
			location.href="about:newtab";
		}
	}
	static checkboxPolChange(target){
		let action="setAttribute";
		if(target.MDCCheckbox.checked)action="removeAttribute";
		let el=target.parentElement.parentElement.querySelector("[data-responsabilidad='aceptar']");
		if(!el)return;
		el[action]("disabled","true");
	}
	static async nearbySliderChange(target){
		let c=Number(target.getAttribute("aria-valuenow"));
		if(isNaN(c)||typeof c!="number")return;
		exports.data.nearbyDist=c*1000;
		if(exports.data.coords)self.exports.data.nearby=await self.exports.api.dbQuery.nearby(exports.data.coords);
	}
	static goShop(){
		if(exports.data.online){
			location.href="http://www.arribes.net/tienda/";
		}
	}
}

if(exports){
	exports.events=events;
}

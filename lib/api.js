"use-strict";

//API de Localización
class LocationAPI{
	constructor(...args){
		exports.api.LocationAPI.checkLocation();
		
		this.defaults={
			enableHighAccuracy: false
		}
		this.options=Object.assign({}, this.defaults);
		if(args[1]){
			for(let op in args[1]){
				this.options[op]=args[1][op];
			}
		}
		this.in=args;
		
		this.watcher=this.setHandler(args[0]);
	}
	
	static checkLocation(){
		if(navigator.geolocation)return true;
		throw new Error("La geolocalización no está soportada por el navegador");
	}
	
	static async location(a){
		this.checkLocation();
		if(!await exports.permissions.location(true)){
			let err=new Error("No puedo acceder a la ubicación del dispositivo. Comprueba los permisos");
			throw new Error(err);
		}
		
		let inicio=performance.now();
		let res= await new Promise((resolver,rechazar)=>{
			navigator.geolocation.getCurrentPosition(resolver, rechazar, a);
		});
		console.log(`Location adquired in ${performance.now()-inicio}ms`);
		return res;
	}
	static async getLocationImg(a){
		let {longitude,latitude}=await this.location(a);
		console.log(longitude,latitude);
		return await exports.util.LocationUtil.getImgFromLocation({longitude,latitude});
	}
	
	
	setHandler(a){
		if(typeof a=="object"){
			var handler=a.onchange;
			var err=a.onerror;
		}else{
			var handler=a;
		}
		
		return navigator.geolocation.watchPosition(handler, err, this.options);
	}
	removeHandler(){
		return navigator.geolocation.clearWatch(this.watcher);
	}
}

//API de la base de datos
class DBQuery{
	constructor(){
		this.driverName="IDB";
		this.lock=this.init();
	}
	async init(){
		let url=`dbdrivers/${this.driverName}.js`;
		this.backend=new (await load(url)).exports.DBDriver();
		await this.backend.lock;
	}
	async nearby(a){
		await this.lock;
		let nby=await this.backend.nearby(a);
		return nby;
	}
	async all(){
		await this.lock;
		return await this.backend.all();
	}
	async localidades(a){
		await this.lock;
		return await this.backend.getLocalidades(a);
	}
	async videoguías(){
		await this.lock;
		return await this.backend.getVideoguías();
	}
	async nearbyFrom(a){
		await this.lock;
		return await this.backend.nearbyFrom(a);
	}
	async trackMove(a){
		await this.lock;
		return await this.backend.trackMove(a);
	}
	async destacado(location){
        await this.lock;
        return await this.backend.destacado(location);
    }
}

//API para notificaciones
class PushNotify{
	constructor(){
		this.methods={
			cordova:this.cordovaNotify,
			worker: this.workerNotify
		};
		this.method="worker";
		if(self.cordova)this.method="cordova";
	}
	async init(){
		let loadBadge=await load("img/icons/icon-72x72.png", ["type: text/plain", "notext", "noprefix"]);
		this.badge=loadBadge.internalURL;
		this.defaults={
			body: "",
			icon: this.icon,
			image: "",
			badge: this.badge,
			actions: [{title: "Llévame", action: "app"}],
			tag: "testpwa"
		};
		await exports.permissions.notification();
	}
	async notify(a, b){
		if(typeof this.lock=="undefined"){
			this.lock=this.init();
		}
		await this.lock;
		return await this.methods[this.method](a,b);
	}
	async cordovaNotify(a, b){
		let opt=this.parse(b);
		await this.lock;
		let notify={
			title: a,
			text: b.body
		};
		if(cordova.platformId=="android")notify.icon=b.icon;
		return cordova.plugins.notification.local.schedule(notify);
	}
	async workerNotify(a, b){
		let opt=this.parse(b);
		await this.lock;
		return await exports.api.workerAPI.notify(a, opt);
	}
	parse(pre){
		let opt=Object.assign({}, this.defaults);
		if(typeof pre!="object")return opt;
		
		Object.keys(pre).forEach(key=>{
			opt[key]=pre[key];
		});
		return opt;
	}
}

//History API
class History{
	constructor(){
		this.history=[];
		this.history.push(location.hash);
	}
	push(a){
		this.history.push(a);
	}
	back(){
		let target=this.history[this.history.length-2];
		location=target;
		return target;
	}
}

//Distance comparing API
class Distance{
	constructor(){
		this.props={
			maxDistance: 20*1000 //En metros
		};
		this.lock=this.init();
	}
	async init(){
		return await load("geolib"); //Ensure that geolib has been loaded
	}
	getDistance(a,b){
		let posA=this.parseCoordinates(a);
		let posB=this.parseCoordinates(b);
		return geolib.getDistance(posA, posB);
	}
	async orderArray(position,array,maxDistance){
		await this.lock;
		position=this.parseCoordinates(position);
		let modificado=array.map(A=>{
			if(!A.coordenadas){
				A.distancia=Infinity;
			}else{
				A.distancia=this.getDistance(position, A.coordenadas);
			}
			return A;
		});
		if(typeof maxDistance!="undefined"){
			modificado=modificado.filter(a=>a.distancia<=maxDistance);
		}
		modificado.sort((a,b)=>{
			return a.distancia-b.distancia;
		});
		return modificado;
	}
	parseCoordinates(a){
		if(!a)return;
		if(typeof a=="string"){
			let split=a.split(",");
			return {latitude: split[0], longitude: split[1]};
		}else if(typeof google!="undefined"&&google.maps&&a instanceof google.maps.LatLng){
			return {latitude: a.lat(), longitude: a.lng()};
		}else if(a.latitude&&a.longitude){
			return a;
		}
	}
}

if(exports){
	exports.api={
		LocationAPI,
		dbQuery: new DBQuery(),
		distance: new Distance(),
		pushNotify: new PushNotify(),
		history:new History()
	};
	exports.api.foundation={
		DBQuery,
		Distance,
		PushNotify
	};
	if(exports.swctl)exports.api.workerAPI= new exports.swctl();
}

class ConnectionAPI{
	constructor(){
		console.warn("Conection API polyfill enabled");
		window.ononline=this.onLineEvt;
		window.onoffline=this.offLineEvt;
		this.props={
			effectiveType: "3g"//Cannot detect it in a reliable way.
		}
		this.event=new Event("ConnectionChange");
	}
	onLineEvt(){
		if(this.connectionChangeEvt)this.connectionChangeEvt(this.event);
	}
	offLineEvt(){
		if(this.connectionChangeEvt)this.connectionChangeEvt(this.event);
	}
	get effectiveType(){
		return this.props.effectiveType;
	}
	set onconnectionchange(fn){
		this.connectionChangeEvt=fn;
	}
}
//Detect browser capabilities
if(!navigator.connection){
	navigator.connection=new ConnectionAPI();
}

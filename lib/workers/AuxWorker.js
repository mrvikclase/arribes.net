//Aux Worker intended to work with cordova
//Requests come from message events, and only for cache-intended assets
let props={
	name: "Arribes.net-v1.0"
};
async function install(){
	let cacheKeys=await caches.keys();
	let queue=[];
	cacheKeys.forEach(cache=>{
		if(cache!=props.name)queue.push(caches.delete(cache));
	});
	await Promise.all(queue);
	return [await Promise.all(queue),await caches.open(props.name)];
}
async function getCache(request){
	let cache=caches.open(props.name);
	return await cache.match(request.url);
}
async function fetchAndCache(request){
	let cacheOpen=caches.open(props.name);
	return fetch(request).then(async resp=>{
		cacheOpen.then(cache=>cache.put(request.url,resp.clone()));
		return resp;
	});
}
async function manageCache(request){
	let cache=await getCache(request);
	if(cache)return cache;
}
self.addEventListener("message", async event=>{
	if(event.data.request){
		await this.installProgress;
		let rt=await manageCache(event.data.request);
		return event.ports[0].postMessage(await rt);
	}else if(event.data.do){
		let rt;
		switch(event.data.do){
			case "awaitInstall":
				rt=await installProgress;
			default:
				rt=false;
		}
		return event.ports[0].postMessage(rt);
	}
});
var installProgress=install();

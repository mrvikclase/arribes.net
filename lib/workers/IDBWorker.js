"use-strict";
var props={
	moduleRoot: (()=>{
		let level=3;
		let rt=location.href.split("/");
		rt.length=rt.length-level;
		rt.push("node_modules");
		return rt.join("/");
	})()
}
importScripts(props.moduleRoot+"/dexie/dist/dexie.js");
var handlers={
	syncDB: async (dbArray)=>{
		self.loading=(async ()=>{
			let shallRefresh={};
			for(let i in dbArray){
				if(!dbArray.hasOwnProperty(i))continue;
				let db=dbArray[i];
				let dbRequest=new Request(db,{
					cache: "reload"
				});
				let cached=await caches.match(db).then(r=>(r)?r.text():undefined);
				let {upstream,upClone}=await fetch(dbRequest).then(async r=>{
					return{
						upClone: r.clone(),
						upstream: await r.text()
					}
				}).catch(e=>{
					console.warn("Failed to fetch database");
					return {upstream:undefined,upClone:undefined};
				});
				if(!upstream)continue;//No upstream, continue with IDB
				try{
					if(!cached){
						shallRefresh[i]=JSON.parse(upstream);
						self.tasks.updateCache(db,upClone);
						continue;
					}
				}catch(e){
					console.error("Tried to parse JSON from upstream but is not readable");
					console.log(upstream);
					if(shallRefresh[i])delete shallRefresh[i];
				}
				
				if(cached==upstream)continue;
				if(cached.length==upstream.length&&props.weakCompare)continue;
				
				try{
					self.tasks.updateCache(db,upClone);
					shallRefresh[i]=JSON.parse(upstream);
				}catch(e){
					console.error("Tried to parse JSON from upstream but is not readable");
					console.log(upstream);
					if(shallRefresh[i]) delete shallRefresh[i];
				}
			}
			return shallRefresh;
		})();
		return await self.loading;
	},
	dbInit: async estructura=>{
		self.creating=(async ()=>{
			self.db=new Dexie("Arribes");
			estructura.forEach(a=>{
				self.db.version(Number(a.version)).stores(a.stores);
			});
		})();
		return true;
	},
	awaitDBInit: async ()=>{
		return await self.creating;
	},
	putDB: async dbV=>{
		await self.creating;
		console.log("Called DB insert for "+dbV.name+" with data: ",dbV.data);
		return await db.transaction("rw", db[dbV.name], async ()=>{
			let table=db[dbV.name];
			let {data}=dbV;
			await table.clear();
			await table.bulkPut(data);
			return data;
		});
	},
	parseDB: async (args)=>{
		let rt={};
		let {dbArray,tasksArray}=args;
		await self.creating;
		for(let i in dbArray){
			if(!dbArray.hasOwnProperty(i))continue;
			let tabla=dbArray[i];
			let taskArray=tasksArray[tabla];
			let start=performance.now();
			let tr=await db.transaction("rw", db[tabla], async ()=>{
				let collection=db[tabla].toCollection();
				collection.modify((item,ref)=>{
					if(!taskArray)return;
					for(let i in taskArray){
						if(!taskArray.hasOwnProperty(i))continue;
						let task=taskArray[i];
						tasks[task](item);
					}
				});
				return await collection.toArray();
			});
			await tr;
			console.log(`Table ${tabla} ready on ${performance.now()-start}ms`);
			rt[tabla]=await tr;
		}
		return rt;
	},
	nocache_request: async url=>{
		if(!url)return;
		if(typeof url=="string"){
			let init={
				cache: "no-store"
			};
			var req=new Request(url,init);
		}else{
			let init=url.init;
			if(url.form){
				init.body=new FormData();
				Object.keys(url.form).forEach(i=>{
					init.body.set(i,url.form[i]);
				});
			}
			var req=new Request(url.url,init)
		}
		return await fetch(req).then(rs=>rs.arrayBuffer());
	}
}
var tasks={
	getBlobUrls: item=>{
		let single=img=>{
			URL.revokeObjectURL(img);
			return URL.createObjectURL(img);
		}
		if(item.imagen){
			item.imgURL=single(item.imagen);
		}
		if(item.imagenes){
			item.imgURLs=[];
			item.imagenes.forEach(img=>{
				item.imgURLs.push(single(img));
			});
		}
	},
	updateCache: async (db,upstream)=>{
		let cacheKeys=await caches.keys();
		let cacheName=cacheKeys[cacheKeys.length-1];
		console.log("Opening "+cacheName);
		if(cacheKeys&&cacheKeys.length){
			let cache=await caches.open(cacheName);
			cache.put(db,upstream);
		}else{
			console.error(cacheKeys);
		}
	}
}
self.addEventListener("message", async event=>{
	if(event.data.do){
		let rt=handlers[event.data.do](event.data.args).catch(e=>{
			console.error(e);
			return {error:e.message};
		});
		return event.ports[0].postMessage(await rt);
	}
});

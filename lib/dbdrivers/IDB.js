class Driver{
	constructor(){
		let host="http://www.gestiondereservas.net"; //FIXME: No va a funcionar en sitios HTTPS
		this.props={
			estructura: {
				path: "dbdrivers/estructura.json"
			},
			host,
			reserveUrl: host,
			imgpath: host,
			trackURL: `${host}/appmovil/log`,
			dbpath: `${host}/appmovil`,
			nearby: `${host}/appmovil/cerca`,
			localidades: `${host}/appmovil/localidades`,
			nearbydist: 200,
			maxPhotos: 6,
			maxTime: 10000,
			worker: "lib/workers/IDBWorker.js",
			videoArgs: "?rel=0"
		};
		if(location.protocol=="https:")this.props.nearby="cerca.json";
		this.defaults={
			coordenadas: "41.1050729,-6.7278361",
			distancia: 200000
		};
		this._nearbyFrom={};
		this.lock=this.init();
	}
	async init(){
		this.worker=new IDBWorker(this.props.worker);
		let url=`${this.props.nearby}?coordenadas=${this.defaults.coordenadas}&d=${this.defaults.distancia}`;
		let start=performance.now();
		if(exports.util.connection.online){
			var {upstreamDB,localidades}=await this.worker.message({do: "syncDB", args: {upstreamDB: url, localidades: this.props.localidades}});
			if(upstreamDB){ //DB Updated, parsing it...
				upstreamDB=await this.parseDB(upstreamDB)
				.catch(e=>{
					console.warn("Error parsing UpstreamDB, falling back to IDB");
					console.error(e);
					return undefined;
				});
			}
			if(localidades){
				localidades=await this.parseLocalidades(localidades)
				.catch(e=>{
					console.error(e);
					console.warn("Error parsing LocalidadesDB, falling back to IDB");
					return undefined;
				});
			}
		}
		this.dbLock=this.dbInit();
		console.log(`Finished primary DB tasks in ${performance.now()-start}`);
		
		this.secDBLock=(async ()=>{
			await this.dbLock;
			if(upstreamDB){ //Insert the updated and parsed DB
				var upstreamDBInsert=this.worker.message({do: 'putDB', args: {
					name: "upstreamDB",
					data: upstreamDB
				}});
			}
			if(localidades){
				var localidadesDBInsert=this.worker.message({do: 'putDB', args:{
					name: "localidadesDB",
					data: localidades
				}});
			}
			
			if(!upstreamDB||!localidades){ //No hay actualizaciones
				let dbArray=[];
				let tasksArray={};
				if(!upstreamDB){
					dbArray.push("upstreamDB");
// 					tasksArray["upstreamDB"]=["getBlobUrls"];
				}
				if(!localidades){
					dbArray.push("localidadesDB");
// 					tasksArray["localidadesDB"]=["getBlobUrls"];
				}
				let process=this.worker.message({do: "parseDB",args: {dbArray,tasksArray}});
				let fn=await process;
				if(fn.upstreamDB)this.data=fn.upstreamDB;
				if(fn.localidadesDB)this.localidades=fn.localidadesDB.sort(this.sortByNameSub);
			}
			
			let videoguías=[
				{
					nombre: "Arribes centro",
					src: "https://www.youtube.com/embed/RGDhjAwbMDg"
				}
			];
			this.videoguías=videoguías.map(item=>{
				if(item.src){
					item.src=item.src+this.props.videoArgs;
				}
				return item;
			});
			
			if(upstreamDBInsert)this.data=await upstreamDBInsert;
			if(localidadesDBInsert)this.localidades=await localidadesDBInsert;
			this.updateCache();
		})();
	}
	async sortByName(a){
		a.sort(this.sortByNameSub);
		return a;
	}
	sortByNameSub(a,b){
		if(a.nombre.substr(0,1)<b.nombre.substr(0,1)){
			return -1;
		}
		return 1;
	}
	async parseLocalidades(a){
		let parseProcess=[];
		for(let i in a){
			if(!a.hasOwnProperty(i))continue;
			parseProcess.push((async ()=>{
				let item=a[i];
				if(item.descripcion){
					item.descripcion=await self.exports.util.TextUtil.purgueHTML(item.descripcion);
					item.textDesc=await self.exports.util.TextUtil.parseHTML(item.descripcion);
				}
				item.imgURLs=item.fotos.map(it=>it=this.props.imgpath+it);
				delete item.fotos;
				if(item.imgURLs.length>this.props.maxPhotos)item.imgURLs.length=this.props.maxPhotos;
				for(let i in item.eventos){
					if(!item.eventos.hasOwnProperty(i))continue;
					let evento=item.eventos[i];
					let txtUtil=exports.util.TextUtil;
					evento.descripcion=await txtUtil.purgueHTML(evento.descripcion);
					evento.textDesc=await txtUtil.parseHTML(evento.descripcion);
					evento.imgURL=this.props.imgpath+evento.imagen;
				}
			})());
		}
		await Promise.all(parseProcess);
		
		a.sort((b,c)=>{
			if(b.nombre.substr(0,1)<c.nombre.substr(0,1)){
				return -1;
			}
			return 1;
		});
		return a;
	}
	async dbInit(){
		let dbStrLoad=await load(this.props.estructura.path, this.props.estructura.flags).then(a=>{
			return a.apply[0];
		}).then(a=>{
			if(a.ok)return a.data;
			throw new Error("La estructura de la base de datos no es válida!");
		});
		let workerDB=await this.worker.message({do: "dbInit", args: dbStrLoad});
		
		await load("dexie"); //No debería hacer falta, pero, ¿y si sí?
		this.db=new Dexie("Arribes");
		let estructura=dbStrLoad;
		estructura.forEach(a=>{
			this.db.version(Number(a.version)).stores(a.stores);
		});
		await workerDB;
	}
	async parseDB(floatDB){
		let descParsePool=[];
		let parsePool=[];
		for(let i in floatDB){
			if(!floatDB.hasOwnProperty(i))continue;
			let item=floatDB[i];
			let fn=(async ()=>{
				if(item.imagen){
					if(item.imagen.match(/^\/thumb-600\/img\/$/)){
						console.warn(item.nombre, "No tiene imagen");
					}else{
						item.imgURL=this.props.imgpath+item.imagen;
					}
				}
				if(item.fotos){
					item.imgURLs=item.fotos.map(im=>{
						if(!im)return;
						im=this.props.imgpath+im;
						return im;
					});
					if(item.imgURLs.length>this.props.maxPhotos)item.imgURLs.length=this.props.maxPhotos;
					delete item.fotos;
				}
				
				//Set icons according to material-design-icons
				let icons=["hotel", "restaurant", "directions_boat", "local_mall", "", "photo_camera"]
				let iconPath=["maps", "maps", "maps", "maps", "", "image"];
				item.icon=icons[Number(item.tipo)-1];
				item.iconPath=iconPath[Number(item.tipo)-1];
				
				//Change type to string
				let types=["Alojamiento", "Restaurantes", "Actividades", "Productos", "Caos", "Qué ver"];
				item.tipo=types[Number(item.tipo)-1];
				
				
				//Change name from 'banos'
				item.baños=Number(item.banos);
				delete item.banos;
				
				//Adjust prop categoria
				if(item.categoria){
					item.categoria.id=Number(item.categoria.id);
					item.categoria.número=Number(item.categoria.numero);
					let icons=["", "star_rate", "star_rate", "tenedor"];
					let nomaterial=[3];
					item.categoria.icon=icons[item.categoria.id];
					item.categoria.icons=[];
					item.categoria.material=true;
					if(nomaterial.includes(item.categoria.id))item.categoria.material=false;
					for(let x=0; x<item.categoria.numero; x++)item.categoria.icons.push(item.categoria.icon);
				}
				
				//Establecer destacado en un boolean
                item.destacado=Boolean(item.destacado);
				
				//Set url for reserve
				if(item.reserva)item.reserva=this.props.reserveUrl+item.reserva;
				
				//Set pretty web name
				if(item.web)item.prettyWeb=(new URL(item.web)).hostname;
				
				//Set navigation URL
				if(item.coordenadas)item.navigationUrl=`https://www.google.com/maps/search/?api=1&query=${item.coordenadas.replace(" ","")}`;
				
				//Set accessible propery
				item.accesible=Boolean(item.accesible);
				
				//Establecer nombre compuesto de provincia
				if(item.provincia&&item.localidad) item["provinciaComp"]=`${item.localidad} (${item.provincia})`;
				
				//Establecer el tipo de alquiler
				if(item.alquiler){
					let tipos=["", "Habitaciones", "Alquiler completo", "Camping", "Cabañas", "Plazas individuales"];
					item.alquiler=tipos[Number(item.alquiler)];
				}
				
				//Establecer características
				item.características={Plazas: item.plazas, ["Hab."]: item.habitaciones, Baños: item.baños};
				let carStr=[];
				Object.keys(item.características).forEach(key=>{
					let salida;
					let val=item.características[key];
					if(typeof val=="string"){
						if(!isNaN(Number(val)))val=Number(val);
					}
					if(!val||val=="0")return;
					let ca=`${key}: ${val}`;
					carStr.push(ca);
				});
				item.carStr=carStr.join(" | ");
				
				//Parse descriptions
				await self.exports.util.TextUtil.parseHTML(item.descripcion).then(parsed=>item.textDesc=parsed);
				await self.exports.util.TextUtil.purgueHTML(item.descripcion).then(parsed=>item.descripcion=parsed);
				return item;
			})();
			parsePool.push(fn);
		}
		let rt=await Promise.all(parsePool);
		return rt;
	}
	async getBlob(a){
		let url=a;
		if(a.href)url=a.href;
		if(!self.cordova){
			return fetch(url).then(a=>{
				if(a.ok)return a.blob();
				throw new Error("Error while fetching");
			});
		}
		return this.getFromCache(url);
	}
	async nearby(a){
		let arr=await self.exports.api.distance.orderArray(a,this.data,exports.data.nearbyDist);
		return arr.map(item=>{
			let salida=item;
			if(item.distancia)salida.distanceStr=this.dist2String(item.distancia);
			return salida;
		});
	}
	async all(){
		await Promise.all([this.lock, this.dbLock, this.secDBLock]);
		return this.data;
	}
	
	//Cache table
	async getFromCache(a){
		await this.dbLock;
		await this.secDBLock;
		let cache=await this.db.transaction("r!", this.db.cache, async ()=>{
			return (await this.db.cache.where("path").equals(a).limit(1).toArray())[0];
		}).catch(e=>{
			console.error("Unable to get the cached response to "+a);
			console.warn(e);
		});
		if(cache)return cache.blob;
		return await this.addToCache(a).catch(e=>{
			console.error("Unable to cache the request "+a);
			console.warn(e);
		});
	}
	async addToCache(a){
		let asset=await fetch(a).then(a=>{
			if(a.ok)return a.blob();
			throw new Error("Fetch failed");
		});
		await this.dbLock;
		await this.secDBLock;
		let transaction=await this.db
		.transaction("rw",this.db.cache, async ()=>{
			return await this.db.cache
			.put({
				path: a,
				blob: asset
			});
		});
		return asset;
	}
	async getLocalidades(a){
		await this.lock;
		if(a){
			await this.secDBLock;
			return await this.db.localidadesDB.where("nombre").equals(a).first();
		}
		return this.localidades||await (async ()=>{
			await this.secDBLock;
			return await this.db.localidadesDB.toArray().then(arr=>{
				return arr.sort(this.sortByName);
			});
		});
	}
	async getVideoguías(){
		await Promise.all([this.lock,this.dblock,this.secDBLock]);
		return this.videoguías;
	}
	async nearbyFrom(loc){
		await Promise.all([this.lock, this.dblock, this.secDBLock]);
		return this._nearbyFrom[loc]||await (async ()=>{
			let res=this.db.transaction("r!", this.db.upstreamDB, this.db.localidadesDB, async ()=>{
				let categorias=exports.data.sitios.filter(item=>item.type=="link").map(i=>i.route.query.cat);
				let localidad=await this.getLocalidades(loc);
				let collection=this.db.upstreamDB.where("localidad").equals(loc);
				let salida=[];
				let queue=[];
				for(let i in categorias){
					if(!categorias.hasOwnProperty(i))continue;
					let tipo=categorias[i];
					queue.push(collection.clone().and(item=>item.tipo==tipo).and(item=>{
						if(!item.coordenadas||item.coordenadas==""){
							item.distance=Infinity; //No coordinates, will be latest
							return true;
						}
						let distance=exports.api.distance.getDistance(item.coordenadas,localidad.coordenadas);
						item.distanceStr=this.dist2String(distance);
						item.distance=distance;
						return true;
					})
					.toArray().then(s=>{
						if(s.length>0)return s.sort((x,z)=>x.distance-z.distance);
						return findNear(tipo,loc,this);
					}));
				}
				for(let i in queue){
					if(!queue.hasOwnProperty(i))continue;
					salida=salida.concat(await queue[i]);
				}
				return salida;
				async function findNear(tipo,loc,_this){
					return await _this.db.upstreamDB.where("tipo").equals(tipo).and(item=>{
						if(!item.coordenadas)exports.data.nocoords.push({nombre:item.nombre,localidad:item.localidad,id:item.id}); //FIXME Solo debug
						if(!item.coordenadas)return false //No coordinates, no filtering, sorry
						
						let dist=exports.api.distance.getDistance(item.coordenadas, localidad.coordenadas);
						if(dist>exports.api.distance.props.maxDistance)return false;
						item.distance=dist;
						if(item.distance)item.distanceStr=_this.dist2String(item.distance);
						return true;
					}).toArray().then(arr=>{
						return arr.sort((i1,i2)=>{
							return i1.distance-i2.distance;
						});
					});
				}
			});
			this._nearbyFrom[loc]=await res;
			return res;
		})();
	}
	dist2String(dist){
		let salida=((d)=>{
			if(d<1000){
				return undefined;
			}else{
				let c=d/1000;
				return `${c.toFixed(1)}Km`;
			}
		})(dist);
		return salida;
	}
	trackMove(loc){
		let form={
			ruta:loc
		};
		let init={
			method: "POST",
			cache: "no-store"
		};
		let req=new Request(this.props.trackURL,init);
		return this.worker.message({do:"nocache_request",args:{init,form,url:this.props.trackURL}})
	}
	async updateCache(){
		if(exports.api.workerAPI){
			let upstream=await this.all();
			let localidades=await this.getLocalidades();
			let salida=[];
			let p1=(async ()=>{
				let proc=[];
				upstream.forEach(item=>{
					proc.push((async ()=>{
						if(item.imgURL)salida.push(item.imgURL);
						if(item.imgURLs)item.imgURLs.forEach(im=>salida.push(im));
					})());
				});
				return await Promise.all(proc);
			})();
			let p2=(async ()=>{
				let proc=[];
				localidades.forEach(loc=>{
					proc.push((async ()=>{
						if(loc.imgURL)salida.push(loc.imgURL);
						if(loc.imgURLs)loc.imgURLs.forEach(im=>salida.push(im));
					})());
				});
				return await Promise.all(proc);
			})();
			return await exports.api.workerAPI.message({
				do:"cache",
				args:salida
			});
		}
	}
	async destacado(loc){
        await Promise.all([this.lock,this.dbLock, this.secDBLock]);
        let arr=(await this.nearbyFrom(loc));
		let filtered=arr.filter(i=>i.localidad==loc);
		if(filtered.length)arr=filtered;
        let id=exports.util.Random.gen(0,arr.length-1);
        return arr[id];
    }
}
class IDBWorker{
	constructor(a){
		this.worker=new Worker(a);
	}
	async message(a){
		if(!this.worker)throw new Error("El Worker no existe");
		let channel=new MessageChannel();
		let rsp=new Promise((resolver,rechazar)=>{
			channel.port1.onmessage=evt=>{
				if(evt.data&&evt.data.error)return rechazar(evt.data.error);
				return resolver(evt.data);
			};
		});
		this.worker.postMessage(a,[channel.port2]);
		return await rsp;
	}
	async destroy(){
		this.worker.destroy();
		delete this.worker;
	}
}
if(exports){
	exports.DBDriver=Driver;
}

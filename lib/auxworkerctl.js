class workerCtl{
	constructor(){
		let loc="lib/workers/AuxWorker.js";
		this.progress=this.init(loc);
	}
	async init(loc){
		this.worker=new Worker(loc);
		return await this.message({do: "awaitInstall"});
	}
	async message(msg){
		await this.progress;
		let channel=new MessageChannel();
		let response=new Promise((resolver, rechazar)=>{
			channel.port1.onmessage=event=>{
				if(event.data&&event.data.error)return rechazar(event.data.error);
				resolver(event.data);
			};
		});
		this.worker.postMessage(msg, [channel.port2]);
		return await response;
	}
	
	async awaitController(){
		console.warn("Shall not be called!");
		return;
	}
	get controllerReady(){
		return true;
	}
	get ready(){
		return this.progress;
	}
	async notify(){
		throw new Error("Worker notify not allowed on AUX!");
	}
}
if(typeof exports !="undefined"){
	exports.swctl=workerCtl;
}

"use-strict";
class swctl{
	constructor(){
		if(!navigator.serviceWorker)throw new Error("El navegador no soporta el ServiceWorker");
		let loc="sw.js";
		this.progress=this.init(loc);
	}
	async init(loc){
		let registration=navigator.serviceWorker.register(loc);
		let controllerWait;
		if(!navigator.serviceWorker.controller)controllerWait=this.awaitController();
		this.registration=await registration;
		if(controllerWait){
			await Promise.race([
				controllerWait,
				new Promise(resolver=>setTimeout(()=>resolver(),10000))
			]);
		}
		return this.registration;
	}
	async notify(...args){
		await this.progress;
		return this.registration.showNotification(args[0], args[1]);
	}
	async message(a){
		await Promise.all([this.progress, this.ready]);
		if(!navigator.serviceWorker.controller)throw new Error("El ServiceWorker no tiene controlador");
		let channel=new MessageChannel();
		let response=new Promise((resolver, rechazar)=>{
			channel.port1.onmessage=event=>{
				if(event.data&&event.data.error)return rechazar(event.data.error);
				resolver(event.data);
			};
		});
		navigator.serviceWorker.controller.postMessage(a, [channel.port2]);
		return await response;
	}
	awaitController(){
		if(this.controllerReady)return Promise.resolve(navigator.serviceWorker.controller);
		return new Promise(resolver=>{
			navigator.serviceWorker.oncontrollerchange=resolver;
		});
	}
	get controllerReady(){
		if(navigator.serviceWorker.controller)return true;
		return false;
	}
	get ready(){
		return navigator.serviceWorker.ready;
	}
}
if(typeof exports!="undefined"){
	exports.swctl=swctl;
}

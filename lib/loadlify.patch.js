var patch={
	whatIs: function (a, b){
		let reg=/(type\: )(.*\/.*)/;
		let res=b.filter(x=>x.match(reg));
		if(res[0]) return res[0].match(reg)[2];

		let type=a.blob.type.match(/.*\/.*/)[0]||a.blob.type;
		try{
			if(a.blob.type=="text/plain"){
				let link=a.link;
				if(a.link.href)link=a.link.href;
				if(link.match(/^(.*\.js)$/))type="application/javascript";
				if(link.match(/^(.*\.json)$/))type="application/json";
				if(link.match(/^(.*\.css)$/))type="text/css";
				if(link.match(/^(.*\.(html|htm))$/))type="text/html";
			}
			if(this.handlers.hasOwnProperty(type)) return type;
		}catch(e){
			console.error(e);
		}
        
        let handledBy;
        Object.keys(this.handlers).forEach(handler=>{
            if(type.match(handler))handledBy=handler;
        });
        if(handledBy)return handledBy;
        
        return "text/plain";
	}
}
exports.loadlifyPatch=patch;

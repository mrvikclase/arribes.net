#!/usr/bin/env node
var config={
    port: process.env.PORT||10000,
    https_port:process.env.HTTPS_PORT||10443,
    root: "/"
};

console.log("==> Loading modules");
const express=require("express");
const http=require("http");

console.log("==> Creating server");
const app=express();
app.use(config.root, express.static(__dirname));

if(process.env.HTTPS_HOME){
	var fs=require("fs");
	var creds={
		key: fs.readFileSync(process.env.HTTPS_HOME+"/server.key", "utf-8"),
		cert: fs.readFileSync(process.env.HTTPS_HOME+"/server.pem", "utf-8")
	};
	var https=require("https");

	https.createServer(creds,app).listen(config.https_port, ()=>console.log("HTTPS server listening on "+config.https_port));
}
var server=http.createServer(app);
server.listen(config.port, ()=>{
	console.log("HTTP server listening on "+config.port);
});

function test(){
	var fs, babel, path;
	try{
		path=require("path");
		fs=require("fs");
		babel=require("babel-core");
	}catch(e){
		console.error(e);
		throw new Error("Uno de los módulos requeridos no está disponible");
	}
	console.log("==> Iniciando test");
	var errors=[];
	let testFilter=/^.*\.js$/;
	let testDir=[".", "lib/", "test/"];
	testDir.forEach(a=>{
		let archivos=fs.readdirSync(a).filter(a=>a.match(testFilter));
		archivos.forEach(b=>{
			let archivo=path.resolve([a, b].join("/"));
			console.log(`Testeando ${archivo}`);
			try{
				babel.analyse(fs.readFileSync(archivo, "UTF-8"));
				console.log(`${archivo} testeado correctamente`);
			}catch(e){
				console.error("Error en "+archivo, e.loc);
				console.log(e.message);
				console.error(e.codeFrame);
				errors.push(e);
			}
		});
	});
	return errors;
}

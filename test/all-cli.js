#!/bin/env node
const path=require("path");
const {test}=require(path.resolve("test/all.js"));
const sass=require("node-sass");
let errors=test(true);
if(errors.length)process.exit(errors.length);
console.log("JavaScript Tests done, no errors");
process.exit(0);

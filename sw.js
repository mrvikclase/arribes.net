var props={
	name: "Arribes.net-v1.1.1",
	list:[
		'index.html', // Página principal
		
		'README.md',//Política de privacidad
		
		'lib/api.js', // Librerías de la app
		'lib/dbdrivers/IDB.js',
		'lib/dbdrivers/estructura.json',
		'lib/events.js',
		'lib/index.js',
		'lib/loadlify-handlers.js',
		'lib/loadlify.patch.js',
		'lib/mdcontrol.js',
		'lib/permissions.js',
		'lib/polyfill.js',
		'lib/swctl.js',
		'lib/theme.css',
		'lib/util.js',
		'lib/workers/IDBWorker.js',
		
		'templates/inicio.vue', //Plantillas
		'templates/nearby.vue',
		'templates/responsabilidad.vue',
		'templates/todo.vue',
		'templates/videoguias.vue',
		'templates/vista.vue',
		
		'components/detalles.vue', //Componentes
		'components/icon-button.vue',
		'components/image-gallery.vue',
		'components/lista.vue',
		'components/mapa.vue',
		
		'node_modules/loadlify/loadlify.min.js', //Módulos de NPM
		'node_modules/vue/dist/vue.min.js',
		'node_modules/vue/dist/vue.js', //FIXME Esto no debe estar en producción
		'node_modules/vue-router/dist/vue-router.js', //FIXME Esto tampoco
		'node_modules/vue-router/dist/vue-router.min.js',
		'node_modules/material-components-web/dist/material-components-web.min.js',
		'node_modules/dexie/dist/dexie.min.js',
		'node_modules/geolib/dist/geolib.js',
		'node_modules/material-design-icons/iconfont/MaterialIcons-Regular.eot',
		'node_modules/material-design-icons/iconfont/MaterialIcons-Regular.woff2',
		'node_modules/material-design-icons/iconfont/MaterialIcons-Regular.woff',
		'node_modules/material-design-icons/iconfont/MaterialIcons-Regular.ttf',
		"node_modules/animate.css/animate.min.css",
		'node_modules/sweetalert2/dist/sweetalert2.all.min.js',
		'node_modules/showdown/dist/showdown.min.js',
		
		'fonts/roboto/roboto.css', //Fuentes para la app
		'fonts/roboto/KFOlCnqEu92Fr1MmSU5fBBc9.ttf',
		'fonts/roboto/KFOmCnqEu92Fr1Mu4mxP.ttf',
		'fonts/roboto/KFOlCnqEu92Fr1MmEU9fBBc9.ttf'
		
	],
	nocache: ["cerca","localidades"],
	cacheProhibit: [],
	allowedProtocols: ["http:", "https:"]
}
function cacheAllowed(a){
	//Check prohibited cache
	if(cacheProhibit(a))return false;
	
	let url=new URL(a);
	for(let i in props.nocache){
		let noc=props.nocache[i];
		if(!props.nocache.hasOwnProperty(noc)) continue;
		if(url.href.match(noc))return false;
	}
	return true;
}
function protoAllowed(a){
	let url=new URL(a);
	if(props.allowedProtocols.includes(url.protocol))return true;
	return false;
}
function cacheProhibit(a){
	//Check protocol
	if(!protoAllowed(a))return true;
	
	let url=new URL(a);
	for(let p in props.cacheProhibit){
		if(!props.cacheProhibit.hasOwnProperty(p))continue;
		if(url.href.match(p))return true;
	}
	return false;
}
function rm(){
	return true;
}
async function handleNotification(event){
	let action;
	try{
		action=(JSON.parse(event.action)).handler;
	}catch(e){
		rm(e);
		action=event.action;
	}
	var actions={
		
	};
	if(!actions.hasOwnProperty(action)){
		let clientList=await clients.matchAll({
			type: "window"
		});
		for (let id in clientList){
			let client=clientList[id];
			if(client.focus)return client.focus();
		}
		return clients.openWindow(location.origin);
	}	
}
self.addEventListener("install", event=>{
	console.log("==> Instalando ServiceWorker");
	event.waitUntil(
		(async ()=>{
			props.stage="install"
			let cache=await caches.open(props.name);
			return Promise.race([
				cache.addAll(props.list).catch(e=>{
					console.warn("Failed while adding files to cache",e);
				}),
				new Promise(resolver=>setTimeout(()=>{
					resolver();
					if(props.stage=="install"){
						console.error("La instalación del ServiceWorker ha tomado demasiado tiempo y se ha interrumpido")
					}
				},10000))
			]);
		})()
	);
});
self.addEventListener("activate", event=>{
	console.log("==> Activando nuevo ServiceWorker");
	event.waitUntil(
		Promise.all(
			[
				(async ()=>{
					props.stage="activate";
					let keys=await caches.keys();
					let w=[];
					keys.forEach(c=>{
						if(c==props.name)return;//Skip current cache
						w.push(caches.delete(c));
					});
					return await Promise.all(w);
				})()
			],
		).then(X=>{
			props.stage="activated";
			return self.clients.claim();
		}).catch(e=>{
			console.error("Ha habido un error al activar el ServiceWorker");
			console.warn(e);
		})
	);
});
self.addEventListener("fetch", event=>{
// 	console.log("==> Petición realizada a ",event.request.url);
	
	if(!protoAllowed(event.request.url)){
		console.log("Proto not allowed for fetch ",event.request.url);
		return event.respondWith(fetch(event.request));
	}
	event.respondWith(cacheControl(event.request));
});
self.addEventListener("notificationclick", event=>{
	console.log("Evento de notificación: ", event);
	event.notification.close();
	event.waitUntil(handleNotification(event));
});
self.addEventListener("message", async event=>{
	let funciones={
		cache: (async a=>{
			let start=performance.now();
			let cache=await caches.open(props.name);
			let salida=[];
			let rt=await (async ()=>{
				let proc=[];
				for(let i in a){
					if(!a.hasOwnProperty(i))continue;
					let url=a[i];
					proc.push((async ()=>{
						let cached=await cache.match(url);
						if(cached)return;
						return await cache.put(url,await fetch(url));
					})());
				}
				return await Promise.all(proc);
			})();
			return await rt;
		})
	};
	if(event.data.do&&funciones.hasOwnProperty(event.data.do)) event.ports[0].postMessage(await funciones[event.data.do](event.data.args));
});
async function cacheControl(request){
	if(!protoAllowed(request.url))return fetchOnly(request);
	
	let cacheMode=request.cache;
	if(cacheMode=="no-store"){
		console.log("no-store mode for "+request.url);
		return fetchOnly(request);
	}
	let cache=await caches.open(props.name);
	let cachedResponse=await cache.match(request);
	switch(cacheMode){
		case "reload":
			return fetchAndCache(request);
			break;
		case "no-cache":
			return fetchAndCache(request).catch(e=>{
				console.error(e);
				if(cachedResponse)return cachedResponse
				throw e;
			});
			break;
		case "force-cache":
			return cachedResponse||fetchAndCache(request);
			break;
		case "only-if-cached":
			if(cachedResponse)return cachedResponse
			throw new Error("Operating in only-if-cached mode, but there isn't any match");
			break;
	}
	if(cachedResponse){
		let allow=cacheAllowed(request.url,request.cache);
		if(!allow)console.log("Cache is not allowed for "+request.url);
		if(allow||!navigator.onLine){
			return cachedResponse;
		}else{
			return await fetchAndCache(request);
		}
	}else{
		if(!navigator.onLine)console.warn("Estamos offline y no está cacheada la página ", request.url);
		if(cacheProhibit(request.url,request.cache))return fetchOnly(request);
		return fetchAndCache(request);
	}
	function fetchOnly(req){
		return fetch(req);
	}
	async function fetchAndCache(req){
		let resp=await fetchOnly(req);
		return saveCache(req.url,resp);
	}
	async function saveCache(a,b){
		if(b.status<200||b.status>299){ //Refuse to cache other responses than 2XX
			if(b.type!="opaque"){ //Don't log opaque responses
				console.log("Refusing to cache "+a+" due to status "+b.status);
			}
			return b;
		}
		let cache=await caches.open(props.name);
		cache.put(a,b.clone());
		return b;
	}
}

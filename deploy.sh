#!/bin/bash
clean(){
	npm ci --only=production
}
compile_scss(){
	npm run build-scss ||npm i --no-save node-sass&&npm run build-scss
}
now_deploy(){
	npm i --no-save now
	npm run deploy
	npm run alias
}
make_tarball(){
	stat lib/theme.css>/dev/null||compile_scss>/dev/null
	if [ "$1" ];then clean; fi
	FILE_NAME="$(basename $(pwd)).tar"
	tar --exclude='.git' --exclude='test' --exclude='deploy.sh' --exclude='*ignore' --exclude='*.yml' \
	--exclude='node_modules/material-design-icons/**.png' --exclude='node_modules/material-design-icons/**.json' \
	--exclude='node_modules/material-design-icons/**.imageset' --exclude="node_modules/@material" \
	-cf "../$FILE_NAME" .
	stat "../$FILE_NAME">/dev/null&&echo "Tarball created on ../$FILE_NAME"
}
if [ "$1" == "staging" ]; then
	echo "Preparing deploy to NOW"
	now_deploy #Clean is not needed, it's done in NOW
	exit 0
fi

if [ "$1" == "production" ]; then
	echo "Deploy to production"
	clean
	exit 0
fi

if [ "$1" == "tarball" ]; then
	echo "Preparing tarball"
	make_tarball "$2"
fi
